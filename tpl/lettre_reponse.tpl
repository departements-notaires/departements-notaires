{# 
Template pour la génération du courrier PDF.
En plus des variables utilisables dans tous les templates (config.xxx), on peut utiliser :
- reference      => concaténation du login de l'étude et de son libellé, et des nom,prénom recherchés
- date_longue"   => date au format long
- corps"         => le résultat de l'évaluation du contenu des templates de réponses
- type_reponse"  => le type de réponse ("reg_pas_aide_recup", "reg_aide_recup","reg_dossier_inconnu")

#}

{# Logo département #}
<p class="logo_haut">
    <img width="240" src=".private/logo_pdf.png" />    
</p>

<br/><br/><br/>

{# Logo du service #}
<img width="300pt" src=".private/service_.png" />


{# Interlocuteur : #}
<p class="interlocuteur">
    <br/><br/>
    Votre interlocuteur : {{config.interlocuteur_pdf}}
    {# Téléphone #}
    <br/>
    <img src=".private/phone.png" style="width:14px;vertical-align:middle" />&nbsp;&nbsp;&nbsp; {{config.telephone_gestion}}
    {# Email  #}
    <br/>
    <img src=".private/envelop.png" style="width:14px;vertical-align:middle" />&nbsp;&nbsp;&nbsp; {{config.mail_gestion}}
</p>

{# Vos ref #}
<p class="reference">
<b>Vos réf. : </b>{{reference}}
</p>

{# Objet #}

<p class="objet">
<br/>
Votre demande d'information sur une éventuelle créance du Département
</p>

{# Ville, Date #}

<p class="date">
<br/>
{{config.ville_service}}, le {{date_longue}}
<p/>

{# Corps #}
<div class="corps">
     {{corps|raw}}
</div>

{# Signature #}
<div class="signature">
    {% if type_reponse=="reg_aide_recup" %}
    <p align="right">Pour le président et par délégation</p>
    {% endif %}
    <p align="right">
        <img width="130" src=".private/signature.png" />    
    </p>
</div>

{# Pied de page #}
<div class="bas_page">
    <p align="center">
        <img width="360" src=".private/baspage_.png" />    
    </p>
</div>

