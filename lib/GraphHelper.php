<?php

JpGraph\JpGraph::load();
JpGraph\JpGraph::module('bar');
JpGraph\JpGraph::module('line');

/**
 * Utilitaire pour générer le graphique des statistiques de recherche et connexion.
 * 
 */
class GraphHelper
{
    private $title;
    /**
     * 
     */
    public function __construct($title = "Evolution des demandes")
    {
        $this->title = $title;
    }
    
    /**
     * Génère l'image des satistiques.
     * Si $dateFrom est supérieur à $dateTo, l'affichage se fait du mois le plus récent (à gauche) au mois le plus ancien
     * (à droite).
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param array $statsRecherche [ ["mois" => "2018-01", "reponse"=>1, "nombre"=>50] [...] ]
     * @param array $statsConnexion [ ["mois" => "2018-01", "nombre"=>50] [...] ]
     * @param string $outputPath chemin du fichier où l'image sera créée. Si null, écrit sur le flux courant
     * @return array tableau contenant les données pour chaque série, les séries étant identifiées par les index
     *  "mois", "demandes", "recuperables", "indus", "inconnus", "ambigus", "connexions", "utilisateurs"
     */
    public function render($dateFrom, $dateTo, $statsRecherche, $statsConnexion, $outputPath=null)
    {
        $series = self::getStatsSeries($dateFrom, $dateTo, $statsRecherche, $statsConnexion);
        
        $this->_render($series['mois'], $series['demandes'], $series['inconnus'], $series['ambigus'], $series['indus'], 
            $series['recuperables'], $series['connexions'], $outputPath);
        
        return $series;
    }

    /**
     * Génère l'image dans un fichier ou sur le stream directement
     * @param array $mois [ "janvier-19", "février-19", ...] 
     * @param array $demandes de taille identique à $mois, total du nombre de demandes pour chaque mois 
     * @param array $inconnus de taille identique à $mois, total du nombre de réponses "inconnu" pour chaque mois 
     * @param array $ambigus de taille identique à $mois, total du nombre de réponses "ambigu" pour chaque mois 
     * @param array $indus de taille identique à $mois, total du nombre de réponses "indu" pour chaque mois 
     * @param array $recuperables de taille identique à $mois, total du nombre de réponses "recuperable" pour chaque mois 
     * @param array $connexions de taille identique à $mois, nombre de connexions utilisateur
     * @param string $outputPath chemin du fichier où l'image sera créée. Si null, écrit sur le stream courant
     */
    private function _render($mois, $demandes, $inconnus, $ambigus, $indus, $recuperables, $connexions, $outputPath)
    {
        $graph = new Graph(1024, 510, 'auto'); 
        $graph->SetScale("textlin");
        $graph->SetColor("#F2F2F2");
        $graph->SetY2Scale("lin",0,900);
        $graph->SetY2OrderBack(false);
        
        $graph->SetMargin(35,50,20,5);
        
        $theme_class = new UniversalTheme;
        $graph->SetTheme($theme_class);
        
        $graph->SetBox(false);
        
        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(array('A','B','C','D'));
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false,false);
        // Setup month as labels on the X-axis
        $graph->xaxis->SetTickLabels($mois);
        
        // Create the bar plots
        // Récupérables
        $b1plot = new BarPlot($recuperables);
        //Indus Probables
        $b2plot = new BarPlot($indus);
        //Ambigus
        $b3plot = new BarPlot($ambigus);
        //Demandes
        $b4plot = new BarPlot($demandes);
        // Inconnus
        $b5plot = new BarPlot($inconnus);
        
        // Connexions
        $lplot = new LinePlot($connexions);
        
        // Create the grouped bar plot
        $gbplot = new AccBarPlot(array($b1plot,$b2plot,$b3plot,$b5plot));
        $gbbplot = new GroupBarPlot(array($b4plot,$gbplot));
        
        // ...and add it to the graPH
        $graph->Add($gbbplot);
        $graph->AddY2($lplot);
        
        // Récupérables
        $b1plot->SetColor("#1D6F99");
        $b1plot->SetFillColor("#1D6F99");
        $b1plot->SetLegend("Récupérables");
        
        //Indus Probables
        $b2plot->SetColor("#FFBEB5");
        $b2plot->SetFillColor("#FFBEB5");
        $b2plot->SetLegend("Indus Probables");
        //Ambigus
        $b3plot->SetColor("#CC646C");
        $b3plot->SetFillColor("#CC646C");
        $b3plot->SetLegend("Ambigus");
        //Demandes
        $b4plot->SetColor("#d2bf64");
        $b4plot->SetFillColor("#d2bf64");
        $b4plot->SetLegend("Demandes");
        $b4plot->value->SetFormat('%d');
        $b4plot->value->Show();
        $b4plot->value->SetColor('#d2bf64');
        //Inconnus
        $b5plot->SetColor("#64A8CC");
        $b5plot->SetFillColor("#64A8CC");
        $b5plot->SetLegend("Inconnus");
        //Connexions
        $lplot->SetBarCenter();
        $lplot->SetColor("black");
        $lplot->SetLegend("Connexions");
        $lplot->mark->SetType(MARK_SQUARE,'',1.0);
        $lplot->mark->SetWeight(2);
        $lplot->mark->SetWidth(5);
        $lplot->mark->setColor("black");
        $lplot->mark->setFillColor("black");
        $lplot->value->SetFormat('%d');
        $lplot->value->Show();
        $lplot->value->SetColor('black');
        
        $graph->legend->SetFrameWeight(1);
        $graph->legend->SetColumns(6);
        $graph->legend->SetColor('#9b9b9b','black');
        
        $band = new PlotBand(VERTICAL,BAND_RDIAG,11,"max",'khaki4');
        $band->ShowFrame(true);
        $band->SetOrder(DEPTH_BACK);
        $graph->Add($band);
        
        $graph->title->Set($this->title);
        
        // Génère
        if ($outputPath != null)
        {
            $graph->Stroke($outputPath);
        }
        else 
        {
            ob_end_clean();
            $graph->Stroke();
        }
    }
    
    /**
     * A partir des données brutes renvoyées par getStatsRecherch() et getStatsConnexion(), renvoie les séries sous forme
     * de tableaux indépendants.
     * Si $dateFrom est supérieur à $dateTo, le tri se fait du mois le plus récent au mois le plus ancien
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param array $statsRecherche [ ["mois" => "2018-01", "reponse"=>1, "nombre"=>50] [...] ]
     * @param array $statsConnexion [ ["mois" => "2018-01", "nombre"=>50] [...] ]
     * @return array tableau contenant les données pour chaque série, les séries étant identifiées par les index
     *  "mois", "demandes", "recuperables", "indus", "inconnus", "ambigus", "connexions", "utilisateurs"
     */
    public static function getStatsSeries($dateFrom, $dateTo, $statsRecherche, $statsConnexion)
    {
        $mois = array();
        $indexDuMois = array();
        
        $intervalUnMois = new DateInterval("P1M"); // 1 mois
        // Déduit les valeurs des mois en fonction des 2 bornes de date
        $i = 0;
        $nbMois = 0;
        
        // Si la date de départ est plus récente, il faut inverser l'ordre des tableaux et le sens du parcours
        if ($dateFrom > $dateTo)
        {
            // On affiche d'abord le mois le plus récent
            $statsRecherche = array_reverse($statsRecherche);
            $statsConnexion = array_reverse($statsConnexion);
            $dateCur = clone $dateFrom; // on part de la date la plus récente, ne pas altérer l'original
            while ($dateCur >= $dateTo)
            {
                $indexDuMois[$dateCur->format("Y-m")] = $i; // on associe pour chaque mois l'index à utiliser dans chaque tableau
                $mois[] = moisEnClair($dateCur->format("n")) . "-" . $dateCur->format("y"); // ex : janvier-18
                // mois précédent
                $dateCur->sub($intervalUnMois);
                $i++;
                $nbMois++;
            }
            // Inverser les indices des mois
            foreach ($indexDuMois as $key=>$value)
            {
                $indexDuMois[$key] = $nbMois - $value -1;
            }
        }
        else
        {
            // On parcourt du mois le plus ancien au plus récent
            $dateCur = clone $dateFrom; // ne pas altérer l'original
            while ($dateCur <= $dateTo)
            {
                $indexDuMois[$dateCur->format("Y-m")] = $i; // on associe pour chaque mois l'index à utiliser dans chaque tableau
                $mois[] = moisEnClair($dateCur->format("n")) . "-" . $dateCur->format("y"); // ex : janvier-18
                // mois suivant
                $dateCur->add($intervalUnMois);
                $i++;
                $nbMois++;
            }
        }
        
        
        // Tous les tableaux doivent avoir la même dimension
        // Dans $all, on utilise l'indice 0 pour stocker le nombre total de demandes, les indices 1 à 4 stockent le nombre de
        // réponses par type recuperables, indus, inconnus, ambigus
        $all = array();
        for ($i=0;$i<=4;$i++) {
            $all[$i] = array();
            for($j=0; $j<$nbMois; $j++) {
                $all[$i][$j] = 0;
            }        
        }
        
        // Parcourt les stats recherche pour renseigner chaque tableau
        foreach ($statsRecherche as $ligne)
        {
            $ligne_mois     = $ligne['mois']; //yyyy-mm
            $ligne_reponse  = $ligne['reponse'];
            $ligne_nombre   = $ligne['nombre'];
            
            if ( array_key_exists($ligne_mois, $indexDuMois) )
            {
                $index = $indexDuMois[$ligne_mois];
                
                $all[0][$index] += $ligne_nombre; // total des demandes
                
                if ($ligne_reponse>=1 && $ligne_reponse<=4)
                {
                    $all[$ligne_reponse][$index] += $ligne_nombre; // total pour chaque type de réponses
                }
            }
        }
        // Parcourt les stats connexion pour renseigner les tableaux $connexions et $utilisateurs
        $connexions = array();
        $utilisateurs = array();
        for ($i=0;$i<=4;$i++) {
            $connexions[$i] = 0;
            $utilisateurs[$i] = 0;
        }
        
        foreach ($statsConnexion as $ligne)
        {
            $ligne_mois         = $ligne['mois']; //yyyy-mm
            $ligne_nombre       = $ligne['nombre'];
            $ligne_different    = $ligne['different'];
            
            if ( array_key_exists($ligne_mois, $indexDuMois) )
            {
                $index = $indexDuMois[$ligne_mois];
                $connexions[$index] += $ligne_nombre;
                $utilisateurs[$index] += $ligne_different;
            }
        }
        
        $demandes       = $all[0];
        $recuperables   = $all[1];
        $indus          = $all[2];
        $inconnus       = $all[3];
        $ambigus        = $all[4];

        // Il faut inverser les indices dans le cas d'un affichage du mois le plus récent au plus ancien
        if ($dateFrom > $dateTo)
        {
            $demandes = self::arrayReverse($demandes);
            $inconnus = self::arrayReverse($inconnus);
            $ambigus = self::arrayReverse($ambigus);
            $indus = self::arrayReverse($indus);
            $recuperables = self::arrayReverse($recuperables);
            $connexions = self::arrayReverse($connexions);
            $utilisateurs = self::arrayReverse($utilisateurs);
        }

        return array(
            "mois"          => $mois,
            "demandes"      => $demandes,
            "inconnus"      => $inconnus,
            "ambigus"       => $ambigus,
            "indus"         => $indus,
            "recuperables"  => $recuperables,
            "connexions"    => $connexions,
            "utilisateurs"  => $utilisateurs,
        );
    }
    
    /**
     * La fonction native array_reverse ne change pas les indices numériques malgré ce qui est écrit dans la doc 
     * @param array $tab
     * @return array les valeurs avec les indices inversés
     */
    static function arrayReverse($tab)
    {
        $ret = array();
        $c = count($tab);
        for($i=0;$i<$c;$i++)
        {
            $ret[$i] = $tab[$c-$i-1];            
        }
        return $ret;
    }

}

