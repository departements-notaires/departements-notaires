<?php
require_once ('notaires_fonctions.php');

sessionCheck();

if( isLoggedIn() ){
	
    $id_recherche = sql_escape(getParam('num_rech'));
    
    // Fixe #45 la consultation de l'historique doit être filtrée :
    // - les profils admin et metier peuvent consutler n'importe quelle recherches
    // - le profil notaire ne peut consulter que ses propres recherches
    $filtre = "";
    if ( !isAdmin() && !isGestionnaire() )
    {
      $login_user = getSession('loginNotaire');
      $filtre = " and lr.nom_etude=" . sql_escape($login_user);
    }
    
    $requete = "Select lr.*, i.sexe from log_recherche lr left join individus i on i.num_ind=lr.id_individus where lr.id_recherche=" . $id_recherche;
    $requete .= $filtre;
    
	$pdf_content = $connect->query($requete);
	$pdf_contenu = $pdf_content->fetch();
	
	// Pas de résultat, on retourne à l'accueil
	if ( !$pdf_contenu || ! isset($pdf_contenu['nom_etude']) )
	{
	   header ('Location: index.php');
	   exit();
	}
	
	// OK, on récupère les données pour générer la lettre
	$userNotaire           = stripslashes($pdf_contenu['nom_etude']);
	$sexeuserNotaire       = $pdf_contenu['sexe'];
	$libelleuserNotaire    = stripslashes($pdf_contenu['libelle']);
	$prenomNotaire         = stripslashes($pdf_contenu['prenom']);
	
	list ($annee_n, $mois_n, $jour_n) = preg_split('/[.\-\/]+/', $pdf_contenu['date_naissance']);
	$date_courrier = $jour_n . '/' . $mois_n . '/' . $annee_n;
	$date_naissance_courrierNotaire = $date_courrier;
	
	$nom_usageNotaire  = stripslashes($pdf_contenu['nom_usage']);
	$mdrNotaire        = stripslashes($pdf_contenu['mdr']);
	$telephoneNotaire  = $pdf_contenu['tel_mdr'];
	$type_reponse      = $pdf_contenu['reponse'];
	
	
	if (isset($sexeuserNotaire)) {
		if ($sexeuserNotaire == "M") {
			$sexe = 'Monsieur';
			$naissance = 'né';
		} else if ($sexeuserNotaire == "F") {
			$sexe = 'Madame';
			$naissance = 'née';
		} else {
			$sexe = "";
			$naissance = 'née';
		}
	} else {
		$sexe = "";
		$naissance = "née";
	}
	
	if ($mdrNotaire == 'ARBRESLE') {
		$mdrNotaire = "L'ARBRESLE";
	}
	
	
	// Paramètres pour la résolution des templates
	$search = [
			"destinataire_etude"  => "",
			"nom"                 => strtoupper($nom_usageNotaire),
			"nom_civil"           => "",
			"prenom"              => $prenomNotaire,
			"prenomd"             => "",
			"date_naissance"      => $date_naissance_courrierNotaire,
			"libelle_notaire"     => "",
			"mail_notaire"        => "",
			"maison_departementale"     => $mdrNotaire,
			"tel_maison_departementale" => $telephoneNotaire,
			"sexe"											=> $sexe,
			"naissance"									=> $naissance,
			
	];
	
	// Génère le PDF
	// Le type de réponse est lu dans l'historique et non pas dans les paramètres de l'URL
	if($type_reponse==1){
		$corps_pdf = templateRender('recup.tpl', $search);
		generer_pdf($_GET['num_rech'], "reg_aide_recup", $corps_pdf);
	}
	else if($type_reponse==2){
		$corps_pdf = templateRender('indus.tpl', $search);
		generer_pdf($_GET['num_rech'], "reg_pas_aide_recup", $corps_pdf);
	}
	else{
		$corps_pdf = templateRender('inconnu.tpl', $search);
		generer_pdf($_GET['num_rech'], "reg_dossier_inconnu", $corps_pdf);
	}
	
	
}else{
	header ('Location: index.php');
}


?>