<?php
/**
 * Exemple d'une extraction de la base IODAS pour alimenter les tables individus et demande.
 *
 * Un email de rapport est envoyé pour chaque table.
 *
 * Le fichier log est complété à chaque extraction pour être affiché dans l'interface de l'administrateur.
 */

include ("../config.php");

// La configuration de la base MYSQL de destination est faite dans config.php (variable globale $connect),
// comme la configuration du serveur d'envoi des emails (variable globale $mail)


// Adresse où le rapport de l'exraction est envoyé
$email_exploit = "notaires.exploit@rhone.fr";

// Configuration BDD IODAS
$host_oracle    = "host";
$port_oracle    = "1526";
$service_oracle = "service";
$user_oracle    = "user";
$pass_oracle    = "pass";

$encoding_oracle = "AL32UTF8";

// Chemin du fichier des logs
$extract_log_file = "./cron.log";


// ====================================================================================================================

$bdd_oracle = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (COMMUNITY = TCP)(PROTOCOL = TCP)(Host = " . $host_oracle . ")
(Port = " . $port_oracle . "))) (CONNECT_DATA =(SERVICE_NAME  = " . $service_oracle . ")))";


$conn_oracle = oci_connect ( $user_oracle, $pass_oracle, $bdd_oracle, $encoding_oracle );

$nb_individus = 0;
$nb_demandes = 0;

if (! $conn_oracle) {
  $e = oci_error ();
  send_mail_exploit ( 'NOTAIRES / Extraction quotidienne / ECHEC',
      'Erreur de connexion base de données '. $service_oracle, $email_exploit);
  
} else {
  // Alimentation de la table individus
  $vidage = 'TRUNCATE TABLE individus;';
  $connect->query ( $vidage );
  // $stmt = $conn->query (
  $stmt = oci_parse ( $conn_oracle, "SELECT I.INDI, I.SEXE, I.NOM, I.NOM_JFIL, I.PREN, I.ANNENAIS, I.MOISNAIS, I.JOURNAIS,
	TRIM(SUBSTR(A.ADRE3,8)) AS SECOURS, U.LIBL AS MDR, U.TELE, U.ADREMAIL, P.LIBL, Q.PROD,CONVERT(I.PREN,'US7ASCII'),
	CONVERT(I.NOM,'US7ASCII') FROM INTERV V JOIN PRODUI P ON P.ID=V.ID_PROD JOIN PROASS Z ON Z.ID_PRODFILS=P.ID
	JOIN NOMENC N ON N.ID=Z.ID_TYPELIEN AND N.TYPO='TPROASSO' AND N.NOME='REGR' JOIN PRODUI Q ON Q.ID=Z.ID_PRODPERE
	JOIN PERI_1 X ON X.APPO=V.APPO JOIN NOMENC M ON M.ID=X.ID_TYPEQUAL AND M.TYPO='QUALINTA' JOIN INDIVI I ON I.INDI=X.INDI
	JOIN ADRESS A ON A.ADRE=V.ADRESECO JOIN TOPTYP Y ON Y.ID_UNTO=A.ID_UNTO JOIN TYPUTE T ON T.ID=Y.ID_TYPEUNTE
	JOIN UNITER U ON U.ID=Y.ID_UNTE WHERE V.TYPETACH='DECINTER' AND V.ANNU='N' AND M.NOME IN ('BENEADMI','BENE')
	AND Q.PROD IN ('1SEXTNONR', '1SEXTREC') AND T.TYPEUNIT='MDR'" );
  oci_execute ( $stmt, OCI_COMMIT_ON_SUCCESS );
  // variable téléphone
  $pnt_trt_tel = array (
      '.',
      '_',
      ' ',
      '-'
  );
  
  // while ( ($row = $stmt->fetch ()) ) {
  while ( ($row = oci_fetch_array ( $stmt, OCI_BOTH )) != false ) {
    $num_ind = $row [0]; // Numéro individu
    $sexe = $row [1]; // Sexe
    $nom_usage = addslashes ( $row [2] ); // Nom d'usage
    // If !isset plutot que If !.
    if (! isset ( $row [3] ))
      $nom_civil = '';
      else
        $nom_civil = addslashes ( $row [3] ); // Nom d'état civil
        
        $prenom = addslashes ( $row [4] );
        
        if (! isset ( $row [5] ))
          $annee_naissance = 1000;
          else
            $annee_naissance = $row [5]; // Année de naissance
            
            if (! isset ( $row [6] ))
              $mois_naissance = 99;
              else
                $mois_naissance = $row [6]; // Mois de naissance
                
                if (! isset ( $row [7] ))
                  $jour_naissance = 99;
                  else
                    $jour_naissance = $row [7]; // Jour de naissance
                    
                    if (! isset ( $row [8] )) {
                      $adresse = 'Adresse non renseignee.';
                    } else {
                      $adresse = addslashes ( $row [8] );
                    } // Addresse secours
                    
                    $mdr = addslashes ( $row [9] ); // MDR
                    
                    if (! isset ( $row [10] ))
                      $telephone = '0800869869'; // téléphone standard de la MDPH
                      else
                        $telephone = str_replace ( $pnt_trt_tel, '', $row [10] ); // Téléphone
                        
                        $mail_mdr = $row [11];
                        $libelle = addslashes ( $row [12] );
                        $code = $row [13];
                        $prenom_sans = addslashes ( $row [14] );
                        $nom_usage_sans = addslashes ( $row [15] );
                        $requete = "INSERT INTO `individus`(id,num_ind,sexe,nom_usage,nom_usage_sans,nom_civil,prenom,prenomd,annee_naissance,
		mois_naissance,jour_naissance,adresse,mdr,telephone,mail_mdr,libelle,code) VALUE(NULL," . $num_ind . ",'" . $sexe . "',
		'" . $nom_usage . "','" . $nom_usage_sans . "','" . $nom_civil . "','" . $prenom . "','" . $prenom_sans . "',
		" . $annee_naissance . "," . $mois_naissance . "," . $jour_naissance . ",'" . $adresse . "','" . $mdr . "',
		'" . $telephone . "','" . $mail_mdr . "','" . $libelle . "','" . $code . "');";
                        $nb_individus ++;
                        
                        $connect->query ( $requete );
  }
  send_mail_exploit ( 'NOTAIRES / Extraction quotidienne / Individus',
      'Extraction réussie</br> Nombre de lignes insérées : ' . $nb_individus, $email_exploit );
  
  // Pour l'alimentation de la table demande
  $vidage1 = 'TRUNCATE TABLE demande;';
  $connect->query ( $vidage1 );
  
  $stmt1 = oci_parse ( $conn_oracle, "SELECT DISTINCT I.INDI, I.SEXE, I.NOM, I.NOM_JFIL, I.PREN, I.ANNENAIS, I.MOISNAIS, I.JOURNAIS,
	CONVERT(I.PREN,'US7ASCII'), CONVERT(I.NOM,'US7ASCII') FROM INTERV V JOIN PRODUI P ON P.ID=V.ID_PROD
	JOIN PROASS Z ON Z.ID_PRODFILS=P.ID JOIN NOMENC N ON N.ID=Z.ID_TYPELIEN AND N.TYPO='TPROASSO' AND N.NOME='REGR'
	JOIN PRODUI Q ON Q.ID=Z.ID_PRODPERE JOIN PERI_1 X ON X.APPO=V.APPO JOIN NOMENC M ON M.ID=X.ID_TYPEQUAL AND
	M.TYPO='QUALINTA' JOIN INDIVI I ON I.INDI=X.INDI JOIN TOPTYP Y ON Y.ID_UNTO=V.ID_UNTOREFE
	JOIN TYPUTE T ON T.ID=Y.ID_TYPEUNTE JOIN UNITER U ON U.ID=Y.ID_UNTE WHERE V.TYPETACH='DDEINTERV' AND V.ANNU='N' AND
	M.NOME IN ('BENEADMI','BENE') AND Q.PROD = '1SEXTREC' AND T.TYPEUNIT='MDR' " );
  oci_execute ( $stmt1, OCI_COMMIT_ON_SUCCESS );
  
  while ( ($row = oci_fetch_array ( $stmt1, OCI_BOTH )) != false ) {
    $num_ind = $row [0]; // Numéro individu
    $sexe = $row [1]; // Sexe
    $nom_usage = addslashes ( $row [2] ); // Nom d'usage
    // If !isset plutot que If !.
    if (! isset ( $row [3] ))
      $nom_civil = '';
      else
        $nom_civil = addslashes ( $row [3] ); // Nom d'état civil
        
        $prenom = addslashes ( $row [4] );
        $prenom_sans = addslashes ( $row [8] );
        if (! isset ( $row [5] ))
          $annee_naissance = 1000;
          else
            $annee_naissance = $row [5]; // Année de naissance
            
            if (! isset ( $row [6] ))
              $mois_naissance = 99;
              else
                $mois_naissance = $row [6]; // Mois de naissance
                
                if (! isset ( $row [7] ))
                  $jour_naissance = 99;
                  else
                    $jour_naissance = $row [7]; // Jour de naissance
                    
                    $nom_usage_sans = addslashes ( $row [9] );
                    $requete1 = "INSERT INTO  `demande`(id,num_ind,sexe,nom_usage,nom_usage_sans,nom_civil,prenom,prenom_sans,
		annee_naissance,mois_naissance,jour_naissance)VALUE(NULL," . $num_ind . ",'" . $sexe . "','" . $nom_usage . "','" . $nom_usage_sans . "',
		'" . $nom_civil . "','" . $prenom . "','" . $prenom_sans . "'," . $annee_naissance . "," . $mois_naissance . "," . $jour_naissance . ");";
                    
                    $nb_demandes ++;
                    $connect->query ( $requete1 );
  }
  send_mail_exploit ('NOTAIRES / Extraction quotidienne / Demandes',
      'Extraction réussie</br> Nombre de lignes insérées : ' . $nb_demandes, $email_exploit );
}

/**
 *
 * @param string $objet
 * @param string $message
 * @param string $email_dest
 */
function send_mail_exploit($objet, $message, $email_dest) {
  global $mail, $extract_log_file;
  
  $jour = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");
  $mois = array("","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
  
  
  $datefr = $jour [date ( "w" )] . " " . date ( "d" ) . " " . $mois [date ( "n" )] . " " . date ( "Y" );
  $dateheure = $datefr . " à " . date ( 'H:i:s' ) . "\n";
  
  $logfile = fopen ( $extract_log_file, 'a+' );
  fputs ( $logfile, $dateheure );
  fputs ( $logfile, $objet . "\n" );
  fputs ( $logfile, $message . "\n" );
  fputs ( $logfile, "\n" );
  fclose ( $logfile );
  
  
  $mail->AddAddress ($email_dest);
  $mail->Subject = $objet;
  $mail->Body = $message;
  
  $mail->Send ();
}

?>